# `dubysa_create-package` #

Creates new composer package inside your Dubysa project. Run it from project directory.

## Syntax ##

    dubysa_create-package vendor_name package_name php_namespace

* `vendor_name` - your company vendor name
* `package_name` - name of new Composer package
* `php_namespace` - PHP namespace all classes will be under

## Installation ##

Install [dubysa/scriptr](https://bitbucket.org/dubysa/scriptr/src/master/) package.

## Overridable Templates ##

* `dubysa/scriptr/composer.json` - composer.json template
* `dubysa/scriptr/readme.md` - readme.md template