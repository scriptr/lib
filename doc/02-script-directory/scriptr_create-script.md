# `scriptr_create-script` #

Creates new script in your composer package. Run it from directory containing composer package.

## Syntax ##

    scriptr_create-script script_name

* `script_name` - file name of script to be created

## Installation ##

This script is always available and doesn't require additional installation.

## Overridable Templates ##

* `scriptr/lib/script` - empty script template
