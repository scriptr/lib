# `dubysa_release-package` #

Pushes new minor version update or patch to server. Run it from directory containing composer package.

## Syntax ##

    dubysa_release-package version

* `version` - new version to be assigned to package. Must follow `vX.Y.Z` notation, where `X` - major version, `Y` - minor version, `Z` - patch number

## Installation ##

Install [dubysa/scriptr](https://bitbucket.org/dubysa/scriptr/src/master/) package.

