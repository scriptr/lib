# `dubysa_create-page` #

Creates new empty page skeleton. Run it from module asset directory.

After page is created, add "meat" to page:

* add views in page layer file
* bind data to views in controller method
* modify route definition in `routes.php`
* handle JS events in JS controller file

## Syntax ##

    dubysa_create-page route_path [php_controller_method] [js_controller]

* `route_path` - URL path which, when requested, renders the page. Example: `/book/edit`  
* `controller_method` - name PHP controller method to handle the route. If omitted, inferred from last segment in URL page, for above example it would be `editPage`
* `js_controller` - name of JS controller to handle JS events. If omitted, inferred from  last segment in URL page, for above example it would be `EditPage`

## Installation ##

Install [dubysa/scriptr](https://bitbucket.org/dubysa/scriptr/src/master/) package.

