# `scriptr_release-package` #

Pushes new minor version update or patch to server according to [Scriptr release process](../release-process). Run it from directory containing composer package.

## Syntax ##

    scriptr_release-package version

* `version` - new version to be assigned to package. Must follow `vX.Y.Z` notation, where `X` - major version, `Y` - minor version, `Z` - patch number

## Installation ##

This script is always available and doesn't require additional installation.

