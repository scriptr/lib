# `scriptr_create-package` #

Creates new composer package inside your Scriptr project. Run it from project directory.

## Syntax ##

    scriptr_create-package vendor_name package_name

* `vendor_name` - your company vendor name
* `package_name` - name of new Composer package

## Installation ##

This script is always available and doesn't require additional installation.

## Overridable Templates ##

* `scriptr/lib/composer.json` - used to generate composer.json file
* `scriptr/lib/readme.md` - used to generate readme.md file