# Release Process #

**Note**. This section is for advanced users, most readers should just use `scriptr_release-package` command.

We recommend (and scripts used below assume that you follow) the following package release process:

* for each release, use semantic versioning `vX.Y.Z` form, where `X` - major version, `Y` - minor version, `Z` - patch number
* empty package, created with `scriptr_create-package` command is uploaded to Git server automatically just after creation with `v0.1.0` version tag
* `master` branch points to latest development
    * initially to `v0.1.0`
    * develop on `master` branch. 
    * before making changes, change dependency in project's `composer.json` to `dev-master` and do `composer update`
    * while working on changes, do the development, commit changes and then use `scriptr_release-package` to make a release
    * if you have no plans for future development, switch back to latest version reported by `composer outdated` using `X.Y.*` constraint and do `composer update`
