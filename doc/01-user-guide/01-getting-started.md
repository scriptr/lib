# Getting Started #

## Installation ##

Run the following commands in shell:

    # go to directory where you will install the project
    cd ~/projects

    # install empty script repository
    composer create-project scriptr/scriptr

Add `vendor/bin` of installed project to system PATH. On Linux, add the following line to the end of `~/.bash_rc`:

    PATH="$PATH:~/projects/scriptr/vendor/bin"

Reopen command line shell.

## Scripts ##

Some scripts come preinstalled, such as `scriptr_create-package`, others require installation of additional Composer packages. Please check [documentation of specific script](scripts/).

## Overridable Templates ##

Some scripts generate files using overridable text templates and mention such templates in their documentation. 

Name of a template is of form `vendor_name/package_name/template_name`. You can find original template in `vendor/vendor_name/package_name/templates/template_name.*` file in directory where Scriptr is installed.

To customize a template, copy original template to `templates/vendor_name/package_name/template_name.*` and modify it there. 
