# Creating Scripts #

## Creating A Package ##

Scripts are organized in Composer packages. For your first script, create new package by running the following commands in shell:

    cd scriptr_directory
    scriptr_create-package vendor_name package_name

## Creating A Script ##

Run the following commands in shell:

    cd vendor/vendor_name/package_name
    git checkout master
    scriptr_create-script script_name

Develop your script is created in `vendor/vendor_name/package_name/bin/script_name`.

After script is developed, update package `readme.md` file (if needed) and script reference documentaion, either internal, or contribute to [Scriptr script catalog](https://bitbucket.org/scriptr/lib).

Commit your changes. 

Release updated package using the following command:

    scriptr_release-package vX.Y.Z

## Modifying Scripts ##

If you decide to change your script, first checkout `master` branch:

    cd vendor/vendor_name/package_name
    git checkout master

Then make the changes, test and update documentation. 

Commit your changes. 

After all, release updated package using the following command:

    scriptr_stable-release vX.Y.Z

