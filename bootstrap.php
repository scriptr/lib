<?php

if (!defined('BP')) {
    define('BP', realpath(__DIR__ . '/../../..'));
}

include BP . '/vendor/autoload.php';

error_reporting(-1);
set_error_handler('handle_error');
ini_set('display_errors', 'Off');
