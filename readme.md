# `scriptr/lib` #

Base library of Scriptr scripts and helper functions. It comes preinstalled wth Scriptr project.

## License ##

This package is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).