<?php
namespace Scriptr\Commands;

use Scriptr\Command;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @property
 */
class ReleasePackage extends Command
{
    protected function default($property) {
        return parent::default($property);
    }

    protected function configure() {
        $this
            ->setDescription('Releases new version of the package')
            ->addArgument('version', InputArgument::OPTIONAL,
                "New version number in form 'vX.Y.Z'")
            ->setHelp(<<<EOT
Before running this command, make sure there are no uncommitted changes in this package.

Run this command in Composer package directory.

This command: 

* 
EOT
            );
    }

    protected function doExecute() {
        // add logic here
    }
}