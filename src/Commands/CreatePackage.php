<?php
namespace Scriptr\Commands;

use Scriptr\Command;
use Scriptr\Exceptions\Abort;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @property string[] $vendor_and_package
 * @property string $vendor_name
 * @property string $package_name
 * @property string $package_path
 * @property string $namespace
 */
class CreatePackage extends Command
{
    protected function default($property) {
        switch ($property) {
            case 'vendor_and_package': return explode('/', $this->input->getArgument('package_name'));
            case 'namespace': return $this->getNamespace();
            case 'vendor_name': return $this->vendor_and_package[0];
            case 'package_name': return $this->getPackageName();
            case 'package_path': return "vendor/{$this->vendor_name}/{$this->package_name}";

        }

        return parent::default($property);
    }

    protected function configure() {
        $this
            ->setDescription('Creates Composer package for Scriptr scripts')
            ->addArgument('package_name', InputArgument::REQUIRED,
                "Package name in form 'vendor/package'")
            ->addArgument('namespace', InputArgument::REQUIRED,
                "PHP namespace")
            ->setHelp(<<<EOT
Before running this command, make sure there are no uncommitted changes in Scriptr project or any of its packages.

Run this command in Scriptr project directory.

This command: 

* creates package's composer.json file
* create package's readme.md file
* puts package files under Git
* pushes package files to Git server
* configures project's composer.json to require newly created package
* runs `composer update` which actually includes new package into the project
EOT
            );
    }

    protected function doExecute() {
        $this->scriptr->makeSureCurrentDirectoryIsProject();

        $this->scriptr->confirmOverwrite($this->package_path);
        $repoUrl = $this->scriptr->require("Create new repository on Github, Bitbucket or Gitlab, " .
            "paste repository URL here and press Enter");

        $this->scriptr->deleteDir($this->package_path);
        mkdir($this->package_path, 0777, true);
        $this->scriptr->cd($this->package_path);

        $this->scriptr->renderFile('composer.json', "{$this->package}/composer.json", [
            'vendor' => $this->vendor_name,
            'package' => $this->package_name,
            'namespace' => json_encode($this->namespace),
        ]);
        $this->scriptr->renderFile('readme.md', "{$this->package}/readme.md", [
            'vendor' => $this->vendor_name,
            'package' => $this->package_name,
            'repoUrl' => $repoUrl,
        ]);

        $this->scriptr->run('git init');
        $this->scriptr->run('git add .');
        $this->scriptr->run('git commit -am "Initial commit"');

        $this->scriptr->run("git remote add origin {$repoUrl}");
        $this->scriptr->run('git push -u origin master');

        $this->scriptr->cd('../../..');

        $this->scriptr->run("composer config repositories.{$this->vendor_name}_{$this->package_name} vcs {$repoUrl}");
        $this->scriptr->run("composer require {$this->vendor_name}/{$this->package_name}:dev-master@dev");
    }

    protected function getPackageName() {
        if (!isset($this->vendor_and_package[1])) {
            $this->output->writeln("Error: package name should be form 'vendor/package'.");
            throw new Abort();
        }

        return $this->vendor_and_package[1];
    }

    protected function getNamespace() {
        $result = $this->input->getArgument('namespace');
        if (strrpos($result, '\\') === strlen($result) - strlen('\\')) {
            return $result;
        }

        return "$result\\";
    }
}