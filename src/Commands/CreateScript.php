<?php

namespace Scriptr\Commands;

use Scriptr\Command;
use Scriptr\Exceptions\Abort;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @property string $script
 * @property string $class_name
 *
 * @property string $script_path
 * @property object $composer
 * @property string $namespace
 * @property string $class_path
 * @property string $full_class_name
 */
class CreateScript extends Command
{
    protected function default($property) {
        switch ($property) {
            case 'script': return $this->input->getArgument('script_name');
            case 'class_name': return $this->input->getArgument('class_name');
            case 'script_path': return "bin/{$this->script}";
            case 'composer': return json_decode(file_get_contents('composer.json'));
            case 'class_path': return "src/Commands/" . strtr($this->class_name, '\\', '/'). ".php";
            case 'namespace': return $this->getNamespace();
            case 'full_class_name': return "{$this->namespace}Commands\\{$this->class_name}";
        }

        return parent::default($property);
    }

    protected function configure() {
        $this
            ->setDescription('Creates Scriptr script')
            ->addArgument('script_name', InputArgument::REQUIRED,
                'File name of script to be created')
            ->addArgument('class_name', InputArgument::REQUIRED,
                'Relative name of the class which will execute the command')
            ->setHelp(<<<EOT
Before running this command, make sure there are no uncommitted changes in Scriptr project or any of its packages.

Run this command in Composer package directory.

This command: 

* creates new Scriptr script file
* creates empty command class file in src/Commands directory where you can put all the logic
* updates composer.json and commit the changes.
* commits the changes and pushed them on Git server
* runs `composer update` which immediately makes new command available for execution
EOT
            );
    }

    protected function doExecute() {
        $this->scriptr->makeSureCurrentDirectoryIsComposerPackage();

        $this->scriptr->confirmOverwrite($this->script_path);
        $this->scriptr->confirmOverwrite($this->class_path);

        $this->scriptr->saveFile('composer.json', $this->renderUpdatedComposerJson());
        $this->scriptr->renderFile($this->script_path, "{$this->package}/script", [
            'full_class_name' => $this->full_class_name,
        ]);
        $this->scriptr->renderFile($this->class_path, "{$this->package}/ScriptClass", [
            'namespace' => $this->namespace,
            'class_name' => $this->class_name,
        ]);

        $this->scriptr->run('git add .');
        $this->scriptr->run('git commit -am "Empty script \'' . $this->script . '\' added"');
        $this->scriptr->run('git push');

        $this->scriptr->cd(BP);
        $this->scriptr->run('composer update');
    }

    protected function renderUpdatedComposerJson() {
        $composer = $this->composer;

        if (!isset($composer->bin)) {
            $composer->bin = [];
        }
        if (!in_array($this->script_path, $composer->bin)) {
            $composer->bin[] = $this->script_path;
        }

        if (!isset($composer->extra)) {
            $composer->extra = new \stdClass();
        }

        if (!isset($composer->extra->scriptr)) {
            $composer->extra->scriptr = new \stdClass();
        }

        if (!isset($composer->extra->scriptr->commands)) {
            $composer->extra->scriptr->commands = new \stdClass();
        }

        $composer->extra->scriptr->commands->{$this->script} = $this->full_class_name;

        return json_encode($composer, JSON_PRETTY_PRINT);
    }

    protected function getNamespace() {
        foreach ($this->composer->autoload->{"psr-4"} as $namespace => $path) {
            if ($path == 'src/') {
                return $namespace;
            }
        }

        $this->output->writeln("Error: 'autoload -> psr-4' section of 'composer.json' should contain " .
            "a namespace of 'src/' directory.");
        throw new Abort();
    }
}