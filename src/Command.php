<?php

namespace Scriptr;

use Scriptr\Exceptions\Abort;
use Scriptr\Exceptions\NotImplemented;
use Scriptr\Traits\LazyPropertyTrait;
use Symfony\Component\Console\Command\Command as BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property Scriptr $scriptr
 * @property InputInterface $input
 * @property OutputInterface $output
 */
abstract class Command extends BaseCommand
{
    use LazyPropertyTrait;

    /**
     * @var string
     */
    public $package;

    public function __construct($name, $package) {
        parent::__construct($name);
        $this->package = $package;
    }

    protected function default($property) {
        global $container; /* @var Container $container */
        switch ($property) {
            case 'input': return $container->get(InputInterface::class);
            case 'output': return $container->get(OutputInterface::class);
            case 'scriptr': return $container->get(Scriptr::class);
        }

        return null;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $container = new Container();

            $config = include BP . '/config.php';
            umask($config['umask'] || 0000);

            $container->set('config', $config);
            $container->set('package', $this->package);

            $container->set(Command::class, $this);
            $container->set(InputInterface::class, $input);
            $container->set(OutputInterface::class, $output);

            $this->doExecute();
            return null;
        }
        catch (Abort $e) {
            return $e->getCode();
        }
    }

    protected function doExecute() {
        throw new NotImplemented();
    }
}