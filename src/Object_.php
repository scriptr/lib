<?php

namespace Scriptr;

use Scriptr\Traits\LazyPropertyTrait;

class Object_
{
    use LazyPropertyTrait;

    /**
     * @param array|object $data
     */
    public function __construct($data = []) {
        foreach ($data as $property => $value) {
            $this->$property = $value;
        }
    }

}