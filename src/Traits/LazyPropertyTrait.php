<?php

namespace Scriptr\Traits;

trait LazyPropertyTrait
{
    public function __get($property) {
        // The following check is not needed by the very definition of __get() magic method. However, as of
        // PHP 7.2, __get() may be called twice in complex isset expression like isset($object->property[$key])
        // This check is added to prevent unwanted side-effects of calculating property value twice
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        return $this->$property = $this->default($property);
    }

    protected function default($property) {
        return null;
    }

    public function __isset($key) {
        return $this->__get($key) !== null;
    }
}