<?php

namespace Scriptr;

class MySql
{
    protected $statements = [];
    /**
     * @var Shell
     */
    protected $shell;
    protected $user;
    protected $requirePassword;
    protected $db;

    public function __construct($shell, $user, $db, $requirePassword = false) {
        $this->shell = $shell;
        $this->user = $user;
        $this->requirePassword = $requirePassword;
        $this->db = $db;
    }

    public function statement($sql, $parameters = []) {
        $this->statements[] = compact('sql', 'parameters');
        return $this;
    }

    public function run($quiet = false) {
        $sql = '';

        foreach ($this->statements as $statement) {
            if ($sql) {
                $sql .= '; ';
            }

            if (!$quiet) {
                echo "mysql: {$statement['sql']}\n";
            }

            foreach ($statement['parameters'] as $parameter => $value) {
                $statement['sql'] = str_replace(":{$parameter}", $value, $statement['sql']);
            }

            $sql .= $statement['sql'];
        }

        $this->shell->command("mysql {$this->credentials()} {$this->db} -e\"{$sql}\"")->run(true);

        return $this;
    }

    protected function credentials() {
        if ($this->requirePassword) {
            echo "Below, enter Mysql '{$this->user}' user password.\n";
        }

        return $this->requirePassword ? "-u {$this->user} -p" : "--login-path={$this->user}";
    }

    public function backup($filename, $quiet = false) {
        $this->shell->command("mysqldump {$this->credentials()} {$this->db} > {$filename}")->run(true);
        if (!$quiet) {
            echo "MySql database '{$this->db}' backed up into '$filename'\n";
        }
        return $this;
    }

    public function restore($filename, $quiet = false) {
        $this->shell->command("mysql {$this->credentials()} {$this->db} < {$filename}")->run(true);
        if (!$quiet) {
            echo "MySql database '{$this->db}' restored from '$filename'\n";
        }
        return $this;
    }

    public function backupZip($filename, $quiet = false) {
        $this->shell->command("mysqldump {$this->credentials()} {$this->db} | gzip > {$filename}")->run(true);
        if (!$quiet) {
            echo "MySql database '{$this->db}' backed up into '$filename'\n";
        }
        return $this;
    }

    public function restoreZip($filename, $quiet = false) {
        $this->shell->command("mysql {$this->credentials()} {$this->db} | gzip < {$filename}")->run(true);
        if (!$quiet) {
            echo "MySql database '{$this->db}' restored from '$filename'\n";
        }
        return $this;
    }

    public function remember($user, $password, $quiet = false) {
        $this->shell->command("mysql_config_editor set --login-path={$user} --host=localhost --user={$user} --password <<< $password")->run(true);
        if (!$quiet) {
            echo "MySql user's '{$user}' password remembered.\n";
        }
        return $this;
    }
}