<?php

namespace Scriptr;

class Container
{
    protected $instances = [];
    protected static $previous_containers = [];

    public function __construct() {
        global $container;

        array_push(static::$previous_containers, $container);
        $container = $this;
    }

    public function __destruct() {
        global $container;

        $container = array_pop(static::$previous_containers);
    }

    public function get($class, ...$args) {
        if (!isset($this->instances[$class])) {
            $this->instances[$class] = new $class(...$args);
        }

        return $this->instances[$class];
    }

    public function set($class, $instance) {
        $this->instances[$class] = $instance;
    }
}