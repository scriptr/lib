<?php

namespace Scriptr;

use Scriptr\Exceptions\Abort;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * @property Command $command
 * @property InputInterface $input
 * @property OutputInterface $output
 * @property QuestionHelper $question_helper
 */
class Scriptr extends Object_
{
    protected function default($property) {
        global $container; /* @var Container $container */
        switch ($property) {
            case 'input': return $container->get(InputInterface::class);
            case 'output': return $container->get(OutputInterface::class);
            case 'command': return $container->get(Command::class);
            case 'question_helper': return $this->command->getHelper('question');
        }

        return parent::default($property);
    }

    public function confirmOverwrite($path) {
        if (!file_exists($path)) {
            return;
        }

        if (!$this->yesNo("'$path' already exists. Overwrite?")) {
            throw new Abort();
        }
    }

    public function makeSureCurrentDirectoryIsComposerPackage() {
        if (!is_file('composer.json') || !is_file('../../../composer.json')) {
            $this->output->writeln(
                "Error: this script should run in a Composer package directory (inside vendor/).");
            throw new Abort();
        }
    }

    public function makeSureCurrentDirectoryIsProject() {
        if (!is_file('composer.json') || is_file('../../../composer.json')) {
            $this->output->writeln(
                "Error: this script should run in a project directory (which contains vendor/).");
            throw new Abort();
        }
    }

    public function yesNo($question) {
        return $this->question_helper->ask($this->input, $this->output, new ConfirmationQuestion($question));
    }

    public function require($question) {
        if (!($answer = trim($this->question_helper->ask($this->input, $this->output, new Question("$question\n"))))) {
            $this->output->writeln("Error: This value is required.");
            throw new Abort();
        }

        return $answer;
    }

    public function saveFile($filename, $contents) {
        if (!is_dir(dirname($filename))) {
            mkdir(dirname($filename), 0777, true);
        }

        file_put_contents($filename, $contents);

        $this->output->writeln("! {$filename}");
    }

    public function renderFile($filename, $template, $variables = []) {
        $this->saveFile($filename, $this->render($template, $variables));
    }

    public function render($__template, $__variables = []) {
        list($__vendor, $__package, $__template) = explode('/', $__template, 3);
        if (!is_file($__filename = BP . "/templates/{$__vendor}/{$__package}/{$__template}.php") &&
            !is_file($__filename = BP . "/templates/{$__vendor}/{$__package}/{$__template}.txt") &&
            !is_file($__filename = BP . "/vendor/{$__vendor}/{$__package}/templates/{$__template}.php") &&
            !is_file($__filename = BP . "/vendor/{$__vendor}/{$__package}/templates/{$__template}.txt"))
        {
            echo "Error: template '$__template' not found.\n";
            exit(1);
        }

        if (strtolower(pathinfo($__filename, PATHINFO_EXTENSION)) != 'php') {
            return file_get_contents($__filename);
        }

        extract($__variables);
        ob_start();

        /** @noinspection PhpIncludeInspection */
        include $__filename;

        return ob_get_clean();
    }

    public function run($command, $quiet = false) {
        if (!$quiet) {
            $this->output->writeln("> {$command}");
        }

        passthru($command, $exitCode);
        if ($exitCode) {
            throw new Abort($exitCode);
        }
    }

    public function cd($path, $quiet = false) {
        if (!$quiet) {
            $this->output->writeln("> cd {$path}");
        }
        chdir($path);
    }

    public function deleteDir($path) {
        if (!is_dir($path)) {
            return;
        }

        foreach (new \DirectoryIterator($path) as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }

            if ($fileInfo->isDir()) {
                $this->deleteDir("{$path}/{$fileInfo->getFilename()}");
            } else {
                unlink("{$path}/{$fileInfo->getFilename()}");
            }
        }

        @rmdir($path);
    }

}