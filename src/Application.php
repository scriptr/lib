<?php

namespace Scriptr;

use Symfony\Component\Console\Application as BaseApplication;

class Application extends BaseApplication
{
    public function extractNamespace($name, $limit = null) {
        $command = $this->all()[$name];
        if ($command instanceof Command) {
            return $command->package;
        }

        return parent::extractNamespace($name, $limit);
    }
}