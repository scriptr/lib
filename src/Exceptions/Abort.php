<?php

namespace Scriptr\Exceptions;

class Abort extends \Exception
{
    public function __construct($code = 1) {
        parent::__construct('', $code);
    }
}