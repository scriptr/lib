<?php

namespace Scriptr;

class LocalShell extends Shell
{
    public function run($quiet = false) {
        foreach ($this->commands as $command) {
            run($command, $quiet);
        }

        $this->commands = [];
        return $this;
    }
}