<?php

use Scriptr\Application;
use Symfony\Component\Console\Input\ArgvInput;

if (!defined('BP')) {
    echo "Error: global constants PP and BP not initialized.";
    exit(1);
}

function readln() {
    $fh = fopen("php://stdin", "r");
    $input = fgets($fh, 1024);
    $input = rtrim($input);
    fclose($fh);

    return $input;
}

function yes_no($message) {
    echo "{$message} [Y, N] ";
    return strtolower(readln()) == 'y';
}

function delete_dir($path) {
    if (!is_dir($path)) {
        return;
    }

    foreach (new \DirectoryIterator($path) as $fileInfo) {
        if ($fileInfo->isDot()) {
            continue;
        }

        if ($fileInfo->isDir()) {
            delete_dir("{$path}/{$fileInfo->getFilename()}");
        } else {
            unlink("{$path}/{$fileInfo->getFilename()}");
        }
    }

    @rmdir($path);
}


function run($command, $quiet = false) {
    if (!$quiet) {
        echo "> {$command}\n";
    }

    passthru($command, $exitCode);
    if ($exitCode) {
        exit($exitCode);
    }
}

function is($command, $quiet = false) {
    if (!$quiet) {
        echo "> {$command}\n";
    }

    passthru($command, $exitCode);

    return $exitCode == 0;
}

function handle_error($level, $message, $file = '', $line = 0, $context = []) {
    if (error_reporting() & $level) {
        throw new ErrorException($message, 0, $level, $file, $line);
    }
}

function studly($value) {
    $value = ucwords(str_replace(['-', '_'], ' ', $value));
    return str_replace(' ', '', $value);
}

function camel($value) {
    return lcfirst(studly($value));
}

function package($path) {
    $result = explode('/', substr(strtr($path, '\\', '/'), strlen(BP . '/vendor/')));
    return "{$result[0]}/{$result[1]}";
}
function create_app() {
    return new Application('Scriptr', 'v1.3.0');
}

function script($class, $path) {
    global $argv;

    $script = basename($argv[0]);

    $application = create_app();

    $application->add(new $class($script, package($path)));
    array_splice($argv, 1, 0, [$script]);

    $application->run(new ArgvInput($argv));
}

function app() {
    $result = create_app();

    foreach (glob(BP . '/vendor/*/*/composer.json') as $filename) {
        if (!($json = json_decode(file_get_contents($filename), true))) {
            continue;
        }

        $package = package($filename);

        foreach (($json['extra']['scriptr']['commands']) ?? [] as $name => $class) {
            $result->add(new $class($name, $package));
        }
    }

    return $result;
}
function in_composer_package() {
}