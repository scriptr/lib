<?php

namespace Scriptr;

abstract class Shell
{
    protected $commands = [];

    public function command($command) {
        $this->commands[] = $command;
        return $this;
    }

    abstract public function run($quiet = false);
}