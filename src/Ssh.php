<?php

namespace Scriptr;

class Ssh extends Shell
{
    protected $server;
    protected $port;
    protected $user;
    protected $keyFile;

    public function __construct($server, $port, $user, $keyFile) {
        $this->server = $server;
        $this->port = $port;
        $this->user = $user;
        $this->keyFile = $keyFile;
    }

    public function upload($localFile, $remoteFile) {
        run("pscp -q -i {$this->keyFile}.ppk -P {$this->port} {$localFile} {$this->user}@{$this->server}:{$remoteFile}", true);
        echo "{$localFile} uploaded to {$remoteFile}\n";
        return $this;
    }

    public function run($quiet = false) {
        $commands = $this->escape(implode(' && ', $this->commands));

        if (!$quiet) {
            foreach ($this->commands as $command) {
                echo ">> $command\n";
            }
        }

        run("ssh -oStrictHostKeyChecking=no -i {$this->keyFile} -p {$this->port} {$this->user}@{$this->server} \"{$commands}\"", true);

        $this->commands = [];
        return $this;
    }

    protected function escape($command) {
        $result = '';

        foreach (preg_split('//u', $command, null, PREG_SPLIT_NO_EMPTY) as $ch) {
            if (mb_strpos('"\\', $ch) !== false) {
                $result .= '\\';
            }
            $result .= $ch;
        }

        return $result;
    }
}