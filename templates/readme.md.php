# `<?php echo $vendor ?>/<?php echo $package ?>` #

## Installation ##

Run the following commands in shell:

    cd scriptr_directory
    composer config repositories.<?php echo $vendor ?>_<?php echo $package ?> vcs <?php echo $repoUrl ?>

    composer require <?php echo $vendor ?>/<?php echo $package ?>:dev-master@dev

## License ##

This package is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).