<?php echo '<?php' ?>

namespace <?php echo $namespace ?>Commands;

use Scriptr\Command;

/**
 * @property
 */
class <?php echo $class_name ?> extends Command
{
    protected function default($property) {
        return parent::default($property);
    }

    protected function configure() {
        $this
            ->setDescription('')
            ->setHelp(<<<EOT
EOT
            );
    }

    protected function doExecute() {
        // add logic here
    }
}