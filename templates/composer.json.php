{
  "name": "<?php echo $vendor ?>/<?php echo $package ?>",
  "license": "MIT",
  "require": {
    "php": ">=7.1",
    "scriptr/lib": ">=1.3"
  },
  "autoload": {
    "psr-4": {
      <?php echo $namespace ?>: "src\/"
    }
  }
}
